FROM alpine:3.12
WORKDIR /ansible-role-docker

COPY . /ansible-role-docker

RUN apk add --no-cache python3 python3-dev py3-pip gcc git curl build-base autoconf automake py3-cryptography linux-headers musl-dev libffi-dev openssl-dev openssh
RUN pip install --upgrade pip && pip install Jinja2==3.0 && pip install --ignore-installed distlib==0.3.1 && pip --proxy "$HTTPS_PROXY" install ansible==2.9.5 && pip install molecule==2.22 && pip install 'molecule[docker]' && pip install 'rich>=10.0.0,<11.0.0'
